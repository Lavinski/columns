function animationManager() {

	var animations = new list();
	var hashset = {};
	
	this.add = add;
	this.update = update;
	
	function add(key, animation) {
	
		// beware the total elapsed will still be reset..
		if (hashset[key]) {
			animations.remove(hashset[key]);
			animation.adjustStart(hashset[key].getNow());
		}
	
		hashset[key] = animation;
		
		animation.key = key;
		animations.add(animation);
		
	}
	
	function update(elapsedTime) {
	
		for(var i = 0; i < animations.count(); i++) {
			var animation = animations[i];
			var key = animation.key;

			animation.update(elapsedTime);
			
			if (animation.done) {
				hashset[key] = null;
				animations.removeAt(i);
				i--;
			}
		}
	}
	
}