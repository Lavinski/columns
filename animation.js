var defaultEasing = 'swing';
function animation(from, to, stepCallback, duration, easing) {

	//var startTime = jQuery.now();
	var totalElapsed = 0;
	var state = 0;
	
	var start = from;
	var end = to;
	var now = start;
	var pos = 0;
	
	var ease = easingTypes[easing || 'swing'];
		
	this.done = false;
	this.update = update;
	this.adjustStart = adjustStart;
	this.getNow = getNow;
	
	function getNow() {
		return now;
	}
	
	function adjustStart(newStart) {
		start = newStart;
	}
	 
	 var counter = 0;
	function update(elapsedTime) {
		//var t = now();
		totalElapsed += elapsedTime;
		
		// t >= duration + this.startTime
		if ( totalElapsed >= duration  ) {
			now = end;
			pos = state = 1;
			
			// done
			this.done = true;
		} else {
			//var totalElapsed = t - startTime;
			state = totalElapsed / duration;
			pos = ease(state, totalElapsed, 0, 1, duration);
		}
		
		now = start + ((end - start) * pos);
		stepCallback(now);
	}
}