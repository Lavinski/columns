/* random functions */

function getViewportHeight() {
	return window.innerHeight ? window.innerHeight : $(window).height();
}

function getViewportWidth() {
	return $(window).width();
}

function tryLog(message){
	if (window.console) {
		console.log(message);
	}
}

function now() {
	return (new Date()).getTime();
}

// these functions are need rework..
//total/duration, totalElapsed, 0, 1, duration
var easingTypes = {
	linear: function( p, n, firstNum, diff ) {
		return firstNum + diff * p;
	},
	swing: function( p, n, firstNum, diff ) {
		return ((-Math.cos(p*Math.PI)/2) + 0.5) * diff + firstNum;
		
	},
	swung: function( p, n, firstNum, diff) {
		return (Math.sin(p*Math.PI/2)) * diff + firstNum;
	}
};