function gestureManager(flicked) {
	
	var opts = {
		flickDist: 100
	};
	
	var start = new vector2();
	var current = new vector2();
	
	var distance = new vector2();
	
	var down = false;
	var startTime = null;
	var lastTime = null;

	var debug = false;
	var showingVis = false;
	var interval = null;
	
	$(window).mousedown(mouseDown); 
	$(window).mouseup(mouseRelease); 
	$(window).mousemove(mouseMove); 
	//$('body').mouseleave(mouseRelease);
	
	var visNode = $('<div disabled>')
		.css('position', 'absolute')
		.css('border', '4px solid black')
		.hide();
	
	function mouseDown(e){
		
		if (!down) {

			//start
			start.X = e.clientX;
			start.Y = e.clientY;
			
			//current
			current.X = e.clientX;
			current.Y = e.clientY;	
		
			$('body')
				.attr('disabled', 'disabled');
				
			if (debug) {
				$('body').append(visNode);	
				var win = $(window);
				
				visNode
					.css('left', win.scrollLeft() + current.X)
					.css('top', win.scrollTop() + current.Y)
					.width(0).height(0).show();
			}
			showingVis = debug;
			interval = setInterval( updateLength, 17 );
			
			distance.X = 0;
			distance.Y = 0;
			

					
			down = true;
			startTime = now();
			lastTime = now();

		}
	}

	function updateLength() {

		var end = now();
		var elapsedTime = end - lastTime;
		lastTime = end;
	
		distance.X = approachZero(elapsedTime, distance.X);
		distance.Y = approachZero(elapsedTime, distance.Y);
		
		if (showingVis) {	
			var width = distance.X,
				height = distance.Y;
			
			var left = (width > 0)? current.X - width : current.X,
				top = (height > 0)? current.Y - height : current.Y;
						
			var win = $(window);
			
			visNode
			.css('left', win.scrollLeft() + left).css('top', win.scrollTop() + top)
			.width(Math.abs(width)).height(Math.abs(height));
		}
	}
	
	function sign(x) {
		if (x == 0) {
			return 0;
		} else if (x > 0) {
			return 1;
		} else {
			return -1;
		}
	}
	
	function approachZero(elapsedTime, x) {
		var out = x + -sign(x) * 500 * elapsedTime / 1000;
		if (sign(out) != sign(x)) {
			return 0;
		} else {
			return out;
		}		
	}
	
	function mouseRelease() {
		// On the release if the last x traval a
		// distance of greater than y
		
		if (down) {
			
			var end = now();
			var t = end - startTime;
			
			if (opts.flickDist < Math.abs(distance.X) || opts.flickDist < Math.abs(distance.Y)) {
				
				if (flicked) {
					var dir = new vector2();
					dir.X = sign(distance.X);
					dir.Y = sign(distance.Y);

					flicked(dir);
				}
			
			}
			
			$('body').removeAttr('disabled');
			down = false;
			
			clearInterval(interval);
			if (showingVis) {
				visNode.hide();
				visNode.remove();
			}
		}
	}

	function mouseMove(e) {
		if (down) {
			var diffx = (e.clientX - current.X);
			var diffy = (e.clientY - current.Y);
			
			current.X = e.clientX;
			current.Y = e.clientY;
			
			distance.X += diffx;
			distance.Y += diffy;
		}
	}

}