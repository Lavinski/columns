var idCounter = 0;
function column() {

	var animationSpeed = 400;
	var node = null;
	var self = this;
	
	var currentSize = new vector2();
	var visualSize = new vector2();
	
	var position = new vector2();
	var visualPosition = new vector2();
	
	var opts = null;
	var manager = null;
	var slideIn = true;
	
	var animations = null;
	var id = idCounter++;
	var closing = false;
	
	/* Constructor */
	(function(){
		
		node = $('<div class="managed col-001"/>').attr('id', 'col' + id).html("<h1 class='text'>0" + (id + 1) + "</h1>");
		animations = new animationManager();
		
	})();
	
	/* public properties */
	this.id = id;
	this.node = node;
	this.currentSize = currentSize;
	this.visualSize = visualSize;
	
	this.position = position;
	this.visualPosition = visualPosition;
	
	this.getPosition = function(){ return position; };
	
	/* public functions */
	this.update = update;
	
	this.attach = attach;
	this.close = close;
	
	this.minimize = function(){};
	this.maximize = function(){};
	
	this.setPosition = setPosition;
	this.setX = setX;
	this.setY = setY;
	
	this.updatePosition = updatePosition;
	this.updateX = updateX;
	this.updateY = updateY;
	this.attached = false;
	
	this.setWidth = setWidth;
	this.setHeight = setWidth;
	this.updateWidth = updateWidth;
	this.updateHeight = updateHeight;
	
	/* functions */
	
	function load(url) {
		// Draw spinner
	}
	
	function getDesiredHeight() {
		return getViewportHeight() - (opts.pageMargin * 2) - 17; // for the scrollbars
	}
	
	function attach(mgr, index){
	
		manager = mgr;
		opts = mgr.opts;
		if (!isNaN(index)) {
			$(manager.root.children()[index]).after(node);
		} else {
			manager.root.append(node);
		}
		
		updateWidth(0);
		updateHeight(0);
		currentSize.X = 0;
		currentSize.Y = 0;
		
		setWidth(opts.columnWidth);
		setHeight(getDesiredHeight()); 
		
		var midScreen = (getViewportHeight() / 2);

		updateY(midScreen); // start at center
		setY(opts.pageMargin); // move up
		
		self.attached = true;
		closing = false;
	}
	
	function close() {
		closing = true;
		setHeight(0);
		setWidth(0);
		setY(getViewportHeight() / 2);
		setTimeout(function(){
			node.remove();
			self.attached = false;
			manager.remove(self);
		}, animationSpeed)
	}
	
	function setHeight(height) {
		currentSize.Y = height;
		animations.add('height', new animation(visualSize.Y, height, function(height){
			updateHeight(height)
		}, animationSpeed, 'swing'));
	}
	
	function updateHeight(height) {
		visualSize.Y = height;
		node.height(height);
	}
	
	function setWidth(width) {
		currentSize.X = width;
		animations.add('width', new animation(visualSize.X, width, function(width){
			updateWidth(width)
		}, animationSpeed, 'swing'));
	}
	
	function updateWidth(width) {
		visualSize.X = width;
		node.width(width);
	}
			
	function setX(x) {
		position.X = x;
		animations.add('posx', new animation(visualPosition.X, x, function(x){
			updateX(x)
		}, animationSpeed, 'swing'));
	}
	
	function setY(y) {
		position.Y = y;
		animations.add('posy', new animation(visualPosition.Y, y, function(y){
			updateY(y)
		}, animationSpeed, 'swing'));
	}
	
	function setPosition(x, y) {
		setX(x);
		setY(y);
	}
	
	
	function updateX(x) {
		
		visualPosition.X = x;
		node.css('left', x);
	}
	
	function updateY(y) {
		
		visualPosition.Y = y;
		node.css('top', y);
	}
	
	function updatePosition(x, y) {
		
		visualPosition.X = x;
		visualPosition.Y = y;
		
		node.css('left', x).css('top', y);
	}
	
	function update(elapsedTime){
		animations.update(elapsedTime);
		
		// if height needs updating
		var desiredHeight = getDesiredHeight();
		if (currentSize.Y != desiredHeight && !closing) {
			setHeight(desiredHeight); 
		}
	}
}