function list() {
	
	var length = 0;
	var self = this;
	
	/* public functions */
	
	this.get = get;
	this.add = add;
	this.remove = remove;
	this.removeAt = removeAt;
	this.count = count;
	this.indexOf = indexOf;
	this.insertAt = insertAt;
	
	/* functions */
	
	function count() {
		return length;
	}
	
	function get(index) {
		return self[index];
	}
	
	function add(obj){
		self[length] = obj;
		length++;
	}
	
	function indexOf(obj) {
		for(var i = 0; i < length; i++) {
			if (self[i] == obj) {
				return i;
			}
		}
		return -1;
	}
	
	function insertAt(obj, index) {
		if (index >= 0 && index <= length) {
			for(var i = length; i > 0; i--) {
				self[i + 1] = self[i];
			}
			self[index] = obj;
			length++;
		}
	}
	
	function remove(obj){
		var index;
		for (index = 0; index < length; index++) {
			if (self[index] == obj) {
				return removeAt(index);
			}
		}
		return false;
	}
	
	function removeAt(index){
		if (index >= 0 && index < (length)) {
			length = length - 1;
			
			var jndex;
			for (jndex = index; jndex < length; jndex++) {
				self[jndex] = self[jndex + 1];
			}
			
			self[jndex] = null;
			return true;
		}
		return false;
	}
}