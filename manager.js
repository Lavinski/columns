function manager(){

	var win = null; 
	var root = null;
	var body = null;
	var opts = null;
	var columns = null;
	var self = this;
	var timer = null;
	var width = 0;
	var gesture = null;
	var animations = new animationManager();
	
	/* Constructor */
	(function(){
		win = $(window);
		body = $('body');
		columns = new list();
		opts = {
			columnWidth: getColumnSize(),
			animationSpeed: 500,
			pageMargin: 20,
			interval: 17,
			leftGap: 100,
		};
		
		root = createRoot();
		
		// setup mouse tracking..
		gesture = new gestureManager(flicked);
		win.resize(resizeColumns);
		
		
		//tick();
	})();
	
	/* properties */
	this.opts = opts;
	
	/* public functions */
	this.root = root;
	this.tick = tick;
	this.add = add;
	this.remove = remove;	
	this.start = tick;
	this.stop = stop;
	this.scrollTo = scrollTo;
	
	/* private functions */
	
	function createRoot() {
		var node = $('<div class="root"/>');
				
		body.append(node);
		return node;
	}
	
	function getColumnSize() {
		
		return Math.max(400, getViewportWidth() * 0.8);
	}
	
	function resizeColumns() {
		opts.columnWidth = getColumnSize();
		for(var i = 0; i < columns.count(); i++) {
			columns[i].setWidth(opts.columnWidth);
		}
	}
	
	function scrollTo(column) {
		
		if (column) {
			var last = win.scrollLeft();
			//column.node.css('border', 'red');
			animations.add('horizonalScroll', new animation(
				last, 
				column.position.X - opts.pageMargin, 	
				function(x) { win.scrollLeft(x) },
				opts.animationSpeed
			));
		} else {
			animations.add('horizonalScroll', new animation(
				win.scrollLeft(),
				0,
				function(x) { win.scrollLeft(x) },
				opts.animationSpeed
			));
		}
	}
	
	function flicked(direction) {
		// get closest window to left
		var index = 0
		var smallestGap = 10000;
			
		for(var i = 0; i < columns.count(); i++) {
			var gap = Math.abs(columns[i].position.X - win.scrollLeft());
			if (gap < smallestGap) {
				index = i;
				smallestGap = gap;
			}
		}

		if (index < columns.count()) {
		
			if (smallestGap < opts.columnWidth) {
				if (direction.X > 0) {
					if (index + 1 < columns.count()) {
						index += 1;
					}
					
				} else {
					if (index - 1 >= 0) {
						index -= 1;
					} else if (index == 0) {
						index = -1;
					}
				}
			} // else align current col
		
			if (index == -1) {
				scrollTo(null);
			} else {
				scrollTo(columns[index]);
			}
		}
	}
	
	function stop() {
		clearInterval(timer);
	}
	
	function tick() {
	/// <summary> This function is the javascript animation loop </summary>
		
		var lastTime = now();		
		timer = setInterval(function() {
			var absTime = now();
			var elapsedTime = absTime - lastTime;
			lastTime = absTime;
			
			update(elapsedTime);
			
		}, opts.interval);
	}
	
	function update(elapsedTime) {
	
		animations.update(elapsedTime);
	
		var spaceUsage = opts.leftGap + opts.pageMargin;
		for(var i = 0; i < columns.count(); i++) {
			var column = columns[i];
			
			column.update(elapsedTime);
			column.updateX(spaceUsage);
			column.position.X = spaceUsage;
			
			spaceUsage += column.visualSize.X + opts.pageMargin;		
		}
		if (width != spaceUsage) {
			root.width(spaceUsage);
			width = spaceUsage;
		}
				
	}
	
	function add(column, after) {
		if (after) {
			var index = columns.indexOf(after);
			columns.insertAt(column, index + 1);
			column.attach(self, index);
		} else {
			columns.add(column);
			column.attach(self);
		}
	}
	
	function remove(column) {
		columns.remove(column);
		if (column.attached) {
			column.close();
		}
	}
}



$(document).ready( function() {

	// Example Usage
	var mgr = new manager();
	mgr.start();
	
	var columnA = new column();
	var columnB = new column();
	var columnC = new column();
	var columnD = new column();
	
	var columnE = new column();
	var columnF = new column();
	var columnG = new column();
	var columnH = new column();
	
	//setTimeout(function(){ mgr.add(columnA) }, 100);
	//setTimeout(function(){ columnA.close() }, 1000);
	//setTimeout(function(){ mgr.add(columnA) }, 2000);
	
	setTimeout(function(){ mgr.add(columnA) }, 100);
	setTimeout(function(){ mgr.add(columnB) }, 200);
	setTimeout(function(){ mgr.add(columnC) }, 300);
	setTimeout(function(){ mgr.add(columnD) }, 400);
	setTimeout(function(){ mgr.add(columnE) }, 500);
	setTimeout(function(){ mgr.add(columnF) }, 600);
	setTimeout(function(){ mgr.add(columnG) }, 700);
	setTimeout(function(){ mgr.add(columnH) }, 800);
	setTimeout(function(){ mgr.scrollTo(columnB); }, 1400 );
	setTimeout(function(){ mgr.scrollTo(columnC); }, 1900 );
	//setTimeout(function(){ mgr.scrollTo(columnD); }, 2500 );
	setTimeout(function(){ mgr.scrollTo(columnB); }, 3000 );
	setTimeout(function(){ mgr.scrollTo(columnA); }, 3500 );
	setTimeout(function(){ columnA.close() }, 4000);
	setTimeout(function(){ columnB.close() }, 4500);
	setTimeout(function(){ columnC.close() }, 5000);
	setTimeout(function(){ columnD.close() }, 5500);
	setTimeout(function(){ columnE.close() }, 6000);
	setTimeout(function(){ columnF.close() }, 6600);
	setTimeout(function(){ columnG.close() }, 7000);
	setTimeout(function(){ columnH.close() }, 7500);

	setTimeout(function(){ mgr.add(columnA) }, 8100);
	setTimeout(function(){ mgr.add(columnB) }, 8300);
	setTimeout(function(){ mgr.add(columnC) }, 8400);
	
	setTimeout(function(){ mgr.scrollTo(columnA) }, 8300);
	
	setTimeout(function(){ columnA.node.html('<h1 class="text">Thanks for watching my javascript slider demo, </br> Now try swiping with the mouse.</h1>');}, 8100);
	
	//setTimeout(mgr.stop, 3000);
});